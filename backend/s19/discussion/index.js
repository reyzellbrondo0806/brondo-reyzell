// Syntax, Statements, and Comments


console.log("We tell the computer to log this in the console");
alert("We tell the computer to display an alert with this message!");

// Statements - in programming are instructions that we tell the computer to perform
// JS Statements usually end with semicolon (;)

// Syntax - it is the set of rules that describes how statements must be constructed

// Comments 
// For us to create a comment, we use Ctrl + /
// We have single line (Ctrl + /) and multi-line comments(Ctrl + Shift + /)

// console.log("Hello!");

/*alert("This is an alert!");
alert("This is another alert!");*/




// Variables
// This is used to contain data

// Declare variables
	// tell our devices that a variableNmae is created and is ready to store data
	// Syntax
	// let/const variableName

	let myVariable;
	console.log(myVariable); 
	// undefined

	// Variables must be declared first before they are used
	// using variables before they are declared will  return an error
	let hello;
	console.log(hello);

	// declaring and initializing variables
	// Syntax
		// let/const variableName = initialValue

	let productName ='desktop computer';
	console.log(productName)

	let productPrice = 18999;
	console.log(productPrice);









































	// Data Types

	// Strings

	let country = "Philippines";
	let province = "Metro Manila";

	// Concatenating Strings

	let fullAddress = province + "," + country;
	console.log(fullAddress);

	let greeting = "I live in the " + fullAddress;
	console.log(greeting);

	// the scape character (\) in strings in combination with other characters can produce different effects

	// "\n" refers to creating a new line in between text

	let mailAddress = "Metro Manila\n\nPhilippines";
	console.log(mailAddress)

	let message = "John's employees went home early";
	console.log(message);
	message = 'John\'s employee went home early';
	console.log(message);

	// Numbers

	let headcount = 26;
	console.log(headcount);
	let grade = 98.7;
	console.log(grade);
	let planetDistance = 2e10;
	console.log(planetDistance);

	// Combine text and strings
	console.log("John's first grade last quarter is " + grade);

	// Boolean
	let isMarried = false;
	let isGoodConduct = true;
	console.log(isMarried);

	console.log("isGoodConduct: " + isGoodConduct);

	// Arrays
	let grades = [98.7,92.1,90.7,98.6];
	console.log(grades);

	// Objects

	let person = {
		fullName: 'Juan Dela Cruz',
		age: 35,
		isMarried: false,
		contact: ["+639123456789","8700"],
		address: {
			houseNumber: '345',
			city: "Manila"
		}
	};
	console.log(person)

	// typeof operator
	console.log(typeof person)

	// Note: Array is a special type of object with methods and functions to manipulate it.

	console.log(typeof grades);



	// null
		// it is used to intentionally express the absence of the value of a variable declaration/initialization.

		let spouse = null

	// undefined
		// this represents the state of a variable that has been declared but without an assigned value

			let fullname;
			console.log(fullName); //undefined