//controller
const User = require("../models/User");
const Course = require("../models/Course")
const bcrypt = require("bcrypt")
const auth = require("../auth")

//Check if the email exists already
/*
	Steps:
	1."find" mongoose method to find duplicate items
	2."then" method to send a response back to the FE application based on the result of the "find" method

*/

module.exports.checkEmailExists = (reqbody)=>{

	return User.find({email:reqbody.email}).then(result=>{

		if(result.length >0){
			return true;
		}
		//no duplicate found
		//the user is not yet registered in the db
		else{
			return false;
		}

	})
}


module.exports.getUser = (userId) => {

    return User.findById(userId).then((userData, error) => {
        if(error){
            let msg = {
                message: "User ID does not exist."
            }
            console.log(error)
            return msg
        } else {
            if(userData == null){
                let msg = {
                    message: "User ID does not exist."
                }
                console.log(error)
                return msg
            } else {
                let data = userData;
                data.password = "";
                
                return data;
            } 

        }
    }).catch(error => {
        let msg = {
            message: "User ID does not exist."
        }
        console.log(error)
        return msg
    });
};

//things added to user.js - routes
//const userController = require("../controllers/user")
//userController.checkEmailExists(req.body)//.then(resultFromController=>res.send(resultFromController))


//User Registration
/*
	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new User to the database
*/


module.exports.registerUser = (reqbody)=>{

	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email: reqbody.email,
		mobileNo: reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})
	.catch(err=>err)
}


module.exports.loginUser = (req,res)=>{

	return User.findOne({email:req.body.email}).then(result=>{
		if(result === null){
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)


			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			else{
				return res.send(false);
			}
		}
	})
	.catch(err=>send(err))
}

module.exports.getProfile = (req,res)=>{

	return User.findById(req.user.id).then(result=>{
		result.password=""
		return res.send(result)
	})
	.catch(err => res.send(err))
}


//Enroll user to a class
/*
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas database
*/

module.exports.enroll = async (req, res) => {

	//to check in the terminal the user id and courseId
	console.log(req.user.id);
	console.log(req.body.courseId);

	//checks if user is an admin and deny the enrollment
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	let isUserUpdated = await User.findById(req.user.id).then(user =>{

		let newEnrollment = {
			courseId: req.body.courseId
		}

		user.enrollments.push(newEnrollment);

		return user.save().then(user => true).catch(err => err.message);
	});

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => err.message);
	})

	if(isCourseUpdated !== true){
		return res.send({ message: isCourseUpdated });
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully"})
	}
}
