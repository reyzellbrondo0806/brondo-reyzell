//[Activity - s45]

//Dependencies and Modules
const Course = require("../models/Course");

//Create a new course
module.exports.addCourse = (req,res) => {

  let newCourse = new Course({

    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  })

  return newCourse.save().then((course,err)=>{

    if(err){
      return res.send(false);
    }else{
      return res.send(true);
    }
  })
  .catch(err => res.send(err))
}

//[End of Activity]



// Retrieve all courses
module.exports.getAllCourses = (req,res) => {
  return Course.find({}).then(result =>{
    return res.send(result)
  })
}


// Retrieve active courses
module.exports.getAllActiveCourses = (req,res) => {
  return Course.find({isActive: true}).then(result =>{
    return res.send(result)
  })
}



// Retrieve specific course
module.exports.getCourse = (req,res) => {
  return Course.findById(req.params.courseId).then(result =>{
    return res.send(result)
  })
}


module.exports.updateCourse = (req,res) =>{

  let updatedCourse = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  }

  return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error)=>{
    if(error){
      return res.send(false);
    }else{
      return res.send(true);
    }
  })
}


module.exports.archiveCourse = (req, res) =>{

  return Course.findByIdAndUpdate(req.params.courseId, {isActive: false}).then(result=>{
    return res.send(true)
  })
}


module.exports.activateCourse = (req, res) =>{

  return Course.findByIdAndUpdate(req.params.courseId, {isActive: true}).then(result=>{
    return res.send(true)
  })
}





