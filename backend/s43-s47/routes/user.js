const express = require("express");
const userController = require("../controllers/user")
const auth = require("../auth");
const { verify, verifyAdmin } = auth;



const router = express.Router();



router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
})

router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

router.post("/login",userController.loginUser);

router.post("/details", verify, userController.getProfile);

// Route to enroll user to a course
router.post("/enroll", verify, userController.enroll);



module.exports = router;