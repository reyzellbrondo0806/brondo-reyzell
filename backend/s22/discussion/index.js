console.log("Hello World!");

// Functions
	// Parameters and Arguments

/*	function printName() {

		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " + nickname);
	};

	printName(); */

	function printName(name) {

		
		console.log("Hi, " + name);
	};

	printName("Reyzell");

	let sampleVariable = "Cardo";

	printName(sampleVariable);



	function checkDivisibilityBy8(num){

		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?")
		console.log(isDivisibleBy8);
	};

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);
	checkDivisibilityBy8(9678);

/*

	Mini-Activity
	check the divisibility of a number by 4
	have one parameter named num

*/


	function checkDivisibilityBy4(num){

		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?")
		console.log(isDivisibleBy4);
	};

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);
	checkDivisibilityBy4(444);


	// Functions as arguments
	// Function parameter can also accept other functions as arguments

	function argumentFunction() {

		console.log("This function was passes as an argumet before the message was printed");
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	};

	// Adding and removing the parentheses "()" impacts the output of JScalling a heavily
	// when a function is used with parantheses "()" it denotes invoking/calling a function

	invokeFunction(argumentFunction);

	// Using multiple parameters

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	};

	createFullName('Juan', 'Dela', 'Cruz');
	createFullName('Cruz', 'Dela', 'Juan');
	createFullName('Juan', 'Dela');
	createFullName('Juan', 'Dela', 'Cruz', 'III');

	// Using variables as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName,middleName,lastName);




	

	function printFriends(friend1, friend2, friend3){

		console.log("My friends are: " + friend1 + friend2 + friend3);
	};

	printFriends("Donna", "Faye", "Lily");


	// Return Statement 

		function returnFullName(firstName,middleName,lastName){

			return firstName + " " + middleName + " " + lastName;
			console.log("This will not be printed");
		}

		let completeName1 = returnFullName("Monkey","D","Luffy");
		completeName2 = returnFullName("Cardo","Tanggol","Dalisay");

		console.log(completeName1 + " is my bestfriend");
		console.log(completeName2 + " is my friend");



// 1
		function getSquareArea(side) {
			return side**2;
		};

		console.log(getSquareArea(5));


// 2
		function addThreeNums(num1,num2,num3){

			return num1+num2+num3;
		};

		console.log(addThreeNums(5,10,15));


// 3
		function getEqualTo100(num){

			return num == 100;
		};

		console.log(getEqualTo100(20));



