console.log("hello")

// [Objects]
// An object is a data type that is used to represent real world objects
// Information stored in objects are represented in a "key:value" pair
// A key is alaso mostly referred to as a "property" of an object
// Different data types may also be stored in an object's property creating complex data structures

// Creating objects using object initializer/literal notation
/*

	Syntax
	let objectName = {
		keyA: valueA,
		keyB: valueB
	}
*/

	let ninja = {
		name: "Naruto",
		village: "Konoha",
		occupation: "Hokage"
	}

	console.log("Result from creating object using initializers/literal notation");
	console.log(ninja);
	console.log(typeof ninja);

	let dog = {
		name: "Whitey",
		color: "white",
		breed: "Chihuahua"
	}


// Creating objects using constructor function
	// Create a reusable function to create several objects that have the same data structure

	function Laptop(name,manufactureDate){
		this.name = name;
		this.manufactureDate = manufactureDate;
	}

	/*

		function ObjectName(keyA,keyB){
			this.keyA = key A;
			this.keyB = keyB;
		}

	*/

	/*
		"this" keyword allows us to assign a new object's properties by associating them with the values received from a constructor function's parameter

	*/

	let laptop1 = new Laptop('Lenovo',2022);
	console.log('Result from creating objects using object constructor');
	console.log(laptop1);

	let myLaptop = new Laptop('MacBook Air',2020);
	console.log('Result from creating objects using object constructor');
	console.log(myLaptop);

	let oldLaptop = Laptop('Portal', 1980);
	console.log('Result from creating objects without using"new" keyword');
	console.log(oldLaptop);


	// mini Activity 1

	// create 3 more instances of our laptop constructor

	let myLaptop1 = new Laptop('Acer',2023);
	console.log("myLaptop1");
	console.log(myLaptop1);

	let myLaptop2 = new Laptop('Huawei',2022);
	console.log("myLaptop2");
	console.log(myLaptop2);

	let myLaptop3 = new Laptop('MSI',2021);
	console.log("myLaptop3");
	console.log(myLaptop3);

	// Create empty objects

	let computer = {};
	let myComputer = new Object();
	console.log(computer);
	console.log(myComputer);

// [Access Object Properties]

	console.log('Result: ' + myLaptop.name);
	console.log('Result: ' + myLaptop.manufactureDate);
	console.log('Result: ' + myLaptop['name']);
	console.log('Result: ' + myLaptop['manufactureDate']);


// Access array objects	 
	// Accessing array elements can also be done using square brackets
	// Accessing object properties using square bracket notation, and array indexes can cause confusion

	let array = [laptop1, myLaptop];

	// square bracket
	console.log(array[0]['name']);

	// dot notation
	console.log(array[0].name);



// [Initialize, Add, Delete, Reassign Object Properties]

	let car = {};

	car.name = 'Honda Civic';
	console.log('Result from adding properties using dot notation:')
	console.log(car);

	// car.number= [1,2,3];
	// console.log(car);

	car['manufacture date'] = 2019;
	console.log(car['manufacture date']);
	// console.log(car.manufacture date);

	console.log(car);

// delete object properties

	delete car['manufacture date'];
	console.log(car);


// reassign object properties

	car.name = 'Dodge Charger R/T';
	console.log(car);


// [Object methods]
	// A method is a function which is a property of an object
	// Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work

	let person = {
		name: 'Cardo',
		talk: function(){
			console.log('Hello, my name is ' + this.name);
		}
	}

	console.log(person)
	person.talk();

	person.walk = function(){
		console.log(this.name + ' walked 25 steps forward')
	}

	person.walk();


	let friend = {
		firstName: 'Nami',
		lastName: 'Misko',
		address: {
			city: 'Tokyo',
			country: 'Japan'
		},
		email: ['nami@sea.com','namimisko@gmail.com'],
		introduce: function(){
			console.log('My name is ' + this.firstName + ' ' + this.lastName)
		}
	}

	friend.introduce();


// [Real World Application od Objects!]
	// create a game that would have several pokemon interact with each other
	// Every pokemon would have the same set of stats, properties and functions

	// Use object literals

	let myPokemon = {
		name: 'Pickachu',
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log('This Pokemon tackled target pokemon');
			console.log("target Pokemon's health is now reduced")
		},
		faint: function(){
			console.log('Pokemon fainted');
		}
	}

	console.log(myPokemon);
	myPokemon.faint();

	// create an object constructor

	function Pokemon(name,level){
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// Methods
		this.tackle = function(target){
			console.log(this.name + 'tackle' + target.name);
			console.log(target.name + "'s health is now reduced to _targetPokemonHealth_")
		}
		this.faint = function(){
			console.log(this.name + ' fainted.');
		}
	}

	let pickachu = new Pokemon("Pickachu",16);
	let rattata = new Pokemon('Rattata',8);
	pickachu.tackle(rattata);
	rattata.faint();




