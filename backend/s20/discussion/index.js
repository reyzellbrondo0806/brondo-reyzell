console.log("Hello World");

// Arithmetic Operators
// +,-,*,/,%

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	// difference
	let diff = x - y;
	console.log("Result of subtraction operator: " + diff);

	// product
	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	// quotient
	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	// remainder
	let remainder = x % y;
	console.log("Result of modulo operator: " + remainder);

// Assignment Operator
// Basic Assignment Operator (=)

let assignmentNumber = 8;

// Addition Assignment Operator

assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log(assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator

assignmentNumber -= 2;
console.log("Result of -+ : " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of *+ : " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of /+ : " + assignmentNumber);

// Multiple Operators 

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas);

// Increment and Decrement
// Operators that add or subtract value by 1 and reassigns the value of the variable

let z = 1;

let increment = ++z;

// the value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
console.log("Result of pre-increment: " + increment)
console.log("Result of pre-increment: " + z);

increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// pre-decrement
// decrease then reassign
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// post-decrement
// reassign the decrease
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Type Coersion
/*
	type coersion is the automatic or implicit conversion of values from one data type to another
*/

let numA = '10';
let numB = 12;

let coersion = numA + numB;
console.log(coersion); //"1012"
console.log(typeof coersion)

let numC = 16;
let numD = 14;
let nonCoersion =  numC + numD;
console.log(nonCoersion)
console.log(typeof nonCoersion)

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// Comparison Operators

let juan = 'juan';

// Equality Operator

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == 'juan');
console.log('juan' == juan);

// Inequality Operator
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log('juan' != juan);

// Strict Equality Operator
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log('juan' === juan);

// Strict Inequality Operator
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);

// Relational Operators

let abc = 50;
let def =65;

let isGreaterThan = abc > def;
let isLessThan = abc < def;
let isGTOrEqual = abc >= def;
let isLTOrEqual = abc <= def;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTOrEqual);
console.log(isLTOrEqual);

let numStr = "30";
console.log(abc > numStr)
console.log(abc < numStr)


